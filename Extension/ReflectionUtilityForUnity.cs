﻿using System;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MyUtility.Attribute.Extension
{
    public class ReflectionUtilityForUnity
    {
        private readonly Type _targetType;
        private object _targetObject;

        public ReflectionUtilityForUnity(Type targetType, object targetObject)
        {
            this._targetObject = targetObject;
            this._targetType = targetType;
        }

        public MethodInfo[] GetAllFunction()
        {
            return _targetType.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static |
                                          BindingFlags.NonPublic);
        }

        public MethodInfo[] GetAllPublicFunction()
        {
            return _targetType.GetMethods(BindingFlags.Public | BindingFlags.Instance | BindingFlags.Static);
        }

        public MethodInfo[] GetAllNonPublicFunction()
        {
            return _targetType.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static);
        }

        public MethodInfo[] FilterMethodsWithVoidType(MethodInfo[] methodInfos)
        {
            return methodInfos.Where(x => x.ReturnType == typeof(void)).ToArray();
        }

        public MethodInfo[] FilterMethodsWithUnityInspectorCompatible(MethodInfo[] methodInfos)
        {
            return methodInfos.Where(x => x.GetParameters().Length <= 1).ToArray();
        }
    }

    public static class ReflectionUtilityExtension
    {
        public static MethodInfo[] FilterMethodsWithUnityInspectorCompatible(this MethodInfo[] methodInfos)
        {
            return methodInfos.Where(x => x.GetParameters().Length <= 1).ToArray();
        }

        public static MethodInfo[] FilterMethodsWithVoidType(this MethodInfo[] methodInfos)
        {
            return methodInfos.Where(x => x.ReturnType == typeof(void)).ToArray();
        }
        public static bool IsVoidMethod(this MethodInfo methodInfo)
        {
            return methodInfo.ReturnType == typeof(void);
        }
        public static bool IsMethodHasNoArguments(this MethodInfo methodInfo)
        {
            return methodInfo.GetParameters().Length == 0;
        }

        public static string GetLogFromMethodInfo(this MethodInfo[] methodInfos)
        {
            var stringBuilder = new StringBuilder();
            foreach (var m in methodInfos)
            {
                m.GetLogFromMethodInfo(stringBuilder);
                stringBuilder.AppendLine();
            }

            return stringBuilder.ToString();
        }

        public static string GetLogFromMethodInfo(this MethodInfo methodInfo)
        {
            var stringBuilder = new StringBuilder();
            methodInfo.GetLogFromMethodInfo(stringBuilder);
            return stringBuilder.ToString();
        }
        static void GetLogFromMethodInfo(this MethodInfo methodInfo, StringBuilder stringBuilder)
        {
            if (methodInfo.IsVoidMethod())
            {
                stringBuilder.Append("void ");
            }
            else
            {
                stringBuilder.Append(methodInfo.ReturnType.Name);
                stringBuilder.Append(" ");
            }

            stringBuilder.Append(methodInfo.Name);

            if (methodInfo.IsMethodHasNoArguments())
            {
                stringBuilder.Append("()");
            }
            else
            {
                stringBuilder.Append("(");
                foreach (var p in methodInfo.GetParameters())
                {
                    stringBuilder.Append(p.ParameterType.Name);
                    stringBuilder.Append(" ");
                }

                stringBuilder.Append(")");
            }
        }
    }
}