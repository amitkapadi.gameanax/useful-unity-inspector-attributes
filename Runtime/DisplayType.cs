﻿using System;
using UnityEngine;
using System.Linq;

namespace MyUtility.Attribute
{
    public class DisplayType : PropertyAttribute
    {
        public Type baseType;

        public bool isInterface = false;

        public DisplayType(Type baseType)
        {
            this.baseType = baseType;
            isInterface = true;
        }

        public DisplayType(Type baseType, bool isInterface)
        {
            this.baseType = baseType;
            this.isInterface = isInterface;
        }

        public enum SearchOption
        {
            NONE = 1 << 1,
            INTERFACE = 1 << 2,
            BASECLASS = 1 << 3
        }
    }


}