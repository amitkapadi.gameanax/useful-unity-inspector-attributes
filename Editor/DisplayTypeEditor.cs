using System;
using System.Linq;
using MyUtility.Attribute;
using UnityEditor;
using UnityEngine;

namespace MyUtility.Attribute.Editor
{
    [CustomPropertyDrawer(typeof(DisplayType))]
    public class DisplayTypeEditor : PropertyDrawer
    {
        const int helpHeight = 30;
        const int textHeight = 16;
        Type[] list;
        string[] listName;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType != SerializedPropertyType.String)
            {
                return;
            }
            EditorGUI.BeginProperty(position, label, property);
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;
            Rect r = position;
            r.height = textHeight;
            if (listName == null)
            {
                DisplayType dt = attribute as DisplayType;
                Type[] types = GetTypes(dt.baseType, dt.isInterface);
                listName = types.Select(x => x.FullName.ToString()).ToArray();
            }
            if (listName != null)
            {
                int index = Array.IndexOf(listName, property.stringValue);
                index = EditorGUI.Popup(r, index, listName);
                if (listName.Length > 0)
                {
                    property.stringValue = index == -1 ? "" : listName[index];
                    property.serializedObject.ApplyModifiedProperties();
                }
                else
                {
                    EditorGUI.HelpBox(r, "No class found from derived Type", MessageType.Error);
                }
            }
            EditorGUI.indentLevel = indent;
            EditorGUI.EndProperty();
        }

        public static Type[] GetTypes(Type interfaceType)
        {

            var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(interfaceType.IsAssignableFrom).Where(z => !z.IsInterface);
            Type[] listTypes = types.ToArray();
            return listTypes;
        }

        public static Type[] GetTypes(Type parentType, bool isInterfaceInclude)
        {
            var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(parentType.IsAssignableFrom).Where(z => !z.IsInterface);
            Type[] listTypes = types.ToArray();
            return listTypes;
        }


        public static Type[] GetTypesWithOption(Type baseType, bool isInterface)
        {
            if (isInterface)
            {
                var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(baseType.IsAssignableFrom).Where(z => !z.IsInterface);
                Type[] listTypes = types.ToArray();
                return listTypes;
            }
            else
            {
                var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(s => s.GetTypes()).Where(baseType.IsAssignableFrom);
                Type[] listTypes = types.ToArray();
                return listTypes;
            }
        }
    }
}
