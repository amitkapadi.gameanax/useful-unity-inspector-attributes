using UnityEditor;
using UnityEngine;

namespace MyUtility.Attribute.Editor
{
    [CustomPropertyDrawer(typeof(DisplayInRaw))]
    public class DisplayInRawEditor : PropertyDrawer
    {
        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label);
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUILayout.BeginHorizontal();
            property.Loop();
            EditorGUILayout.EndHorizontal();
            //base.OnGUI(position, property, label);
        }
    }
    
    public static class EditorExtensionMethods
    {
        public static void Loop(this SerializedProperty prop)
        {
            if (prop.NextVisible(true))
            {
                do
                {
                    EditorGUILayout.PropertyField(prop, true);
                } while (prop.NextVisible(false));
            }
        }
    }
}